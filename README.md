# synigency
#the way we us to run the code
#python3 manage.py runserver
#http://127.0.0.1:8000/medlyft/

##Medlyft front-end

Below are some React commands that will be useful during the development of the front end
`npm start`
> Starts the development server.

`npm run build`
> Bundles the app into static files for production.

`npm test`
> Starts the test runner.

`npm run eject`
> Removes this tool and copies build dependencies, configuration files and scripts into the app directory. If you do this, you can’t go back!

###To run the front end of the project
`cd medlyft-front` && `cd medlyft-front`
